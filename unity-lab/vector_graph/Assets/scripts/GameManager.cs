﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	//Points
	public GameObject pointA;
	public GameObject pointB;
	public GameObject pointC;

	//Line renderer
	public LineRenderer line_render;

	//posiciones polyLine
	public Vector3[] posiciones;
	public GameObject[] points;
	public Color lineColor = Color.white;
	public List<Vector3> path = new List<Vector3>();



	void Start () {

		line_render = this.gameObject.AddComponent<LineRenderer>();

	}

	void DrawRenderLines(){

		//DRAWING RENDER LINES
		//lineToObject


		//ListarPoints (points);
		int i=0;

		foreach (GameObject go_pos in points)
		{

			line_render.SetPosition (i, go_pos.transform.position);
			i++;
			line_render.positionCount = i+1;
			Debug.Log (i);

		}


		line_render.startWidth = 0.2f;
		line_render.endWidth = 0.2f;

		line_render.useWorldSpace = true;

		line_render.startColor = lineColor;
		line_render.endColor = lineColor;



	}


	void DrawLines(){

		//Lines
		Debug.DrawLine (pointA.transform.position, pointB.transform.position, Color.red);
		Debug.DrawLine (pointB.transform.position, pointC.transform.position, Color.blue);

	}
	
	// Update is called once per frame
	void Update () {

		DrawLines ();
		DrawRenderLines ();

	}
}

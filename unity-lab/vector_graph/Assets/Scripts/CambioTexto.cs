﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CambioTexto : MonoBehaviour {

	//
	public Text textico;

	enum idioma{esp, eng, por};

	//public idioma idiomavar;
	public string idiomavar_string;

	// Use this for initialization
	void Start () {

		//Debug.Log (idiomavar.ToString());

		//idiomavar_string = idiomavar.ToString ();

		switch(idiomavar_string)
		{
		case "spa":
			textico.text = "Hola";
			break;

		case "eng":
			textico.text = "Hello";
			break;

		case "por":
			textico.text = "Olá";
			break;

		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System;
using System.Linq;
using System.Xml;

public class BuildScript : MonoBehaviour {

	//Variables de Android
	public string androidKeystorePass = "";
    public string androidKeyaliasName = "";
    public string androidKeyaliasPass = "";

	public string idioma = "";

	string[] arguments = Environment.GetCommandLineArgs();

	public string fileName = "";
	public string url_aula = "";
	public bool ModoAula;

	//Build StandAlone 64
	[MenuItem("Build/Build Specific/Build Win64-SPA")]
	static void BuildWin_SPA()
	{

		//idioma = "spa";

		GameObject.FindObjectOfType<CambioTexto> ().idiomavar_string = "spa";

		PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
		PlayerSettings.productName = "Español 3D";//Nombre de la aplicación
		PlayerSettings.applicationIdentifier = "com.genmedia.cienciasesp";

        //DefaultBuild(BuildTarget.StandaloneWindows64);
		string[] scenes = { "Assets/Scenes/mainscene.unity" };
		string rutaDrive = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop)+"/"+"Export/"+ PlayerSettings.productName + "/";
		Debug.Log (rutaDrive);
        string nombre = "Player";
		BuildPipeline.BuildPlayer(scenes, rutaDrive + nombre, BuildTarget.StandaloneWindows, BuildOptions.None);
    }

	//Build StandAlone 64
	[MenuItem("Build/Build Specific/Build Win64-ENG")]
	static void BuildWin_ENG()
	{
		GameObject.FindObjectOfType<CambioTexto> ().idiomavar_string = "eng";

		PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
		PlayerSettings.productName = "English 3D";//Nombre de la aplicación
		PlayerSettings.applicationIdentifier = "com.genmedia.cienciasesp";

		//DefaultBuild(BuildTarget.StandaloneWindows64);
		string[] scenes = { "Assets/Scenes/mainscene.unity" };
		string rutaDrive = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop)+"/"+"Export/"+ PlayerSettings.productName + "/";
		Debug.Log (rutaDrive);
		string nombre = "Player";
		BuildPipeline.BuildPlayer(scenes, rutaDrive + nombre, BuildTarget.StandaloneWindows, BuildOptions.None);
	}

	//Build OSX SPA
	[MenuItem("Build/Build Specific/Build OSX-SPA")]
	static void BuildOSX_SPA()
	{
		//GameObject.FindObjectOfType<CambioTexto> ().idiomavar_string = "spa";

		PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
		PlayerSettings.productName = "Español 3D";//Nombre de la aplicación
		PlayerSettings.applicationIdentifier = "com.genmedia.cienciasesp";

		//DefaultBuild(BuildTarget.StandaloneWindows64);
		string[] scenes = { "Assets/Scenes/mainscene.unity" };
		string rutaDrive = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop)+"/"+"Export/"+ PlayerSettings.productName + "/";
		Debug.Log (rutaDrive);
		string nombre = "Player";
		BuildPipeline.BuildPlayer(scenes, rutaDrive + nombre, BuildTarget.StandaloneOSX, BuildOptions.None);
	}

	//Build OSX ENG
	[MenuItem("Build/Build Specific/Build OSX-ENG")]
	static void BuildOSX_ENG()
	{
		//GameObject.FindObjectOfType<CambioTexto> ().idiomavar_string = "eng";

		PlayerSettings.companyName = "Innovative Education";//cambio de Compañia si es necesario
		PlayerSettings.productName = "English 3D";//Nombre de la aplicación
		PlayerSettings.applicationIdentifier = "com.genmedia.cienciasesp";

		//DefaultBuild(BuildTarget.StandaloneWindows64);
		string[] scenes = { "Assets/Scenes/mainscene.unity" };
		string rutaDrive = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Desktop)+"/"+"Export/"+ PlayerSettings.productName + "/";
		Debug.Log (rutaDrive);
		string nombre = "Player";
		BuildPipeline.BuildPlayer(scenes, rutaDrive + nombre, BuildTarget.StandaloneOSX, BuildOptions.None);
	}



	//Leer xml
	void ProcessDirectory(DirectoryInfo aDir)
	{
		var files = aDir.GetFiles().Where(f => f.Extension == ".xml").ToArray();
		foreach(var _fileName in files)
		{
			if(_fileName.Name.Equals("aula_conf.xml"))
			{
				fileName = _fileName.FullName;
				break;
			}
		}

		if (!fileName.Equals("")) 
		{
			openXML();
		}
	}

	public void openXML()
	{
		try
		{
			XmlDocument newXml = new XmlDocument();
			newXml.Load(fileName);
			XmlNodeList _list = newXml.ChildNodes[0].ChildNodes;

			for (int i = 0; i < _list.Count; i++)
			{
				if (_list[i].Name.Equals("url_aula"))
				{
					url_aula = _list[i].InnerText;
					fileName = fileName + " - " + url_aula;
				}

				if (_list[i].Name.Equals("aula"))
				{
					Debug.Log(_list[i].InnerText);
					if (_list[i].InnerText.Equals("true"))
					{
						ModoAula = true;
					}
					else
					{
						ModoAula = false;
					}

					fileName = fileName + " - " + ModoAula.ToString();
				}
			}

			Debug.Log("Hello - Sim-xml true");
		}
		catch (Exception e)
		{
			Debug.Log("file not exist" + e);
			Debug.Log("Hello - Sim-xml catch" + e);
		}
	}

	public void CargarExternalXML()
	{

		fileName = "";
		#if UNITY_IPHONE
		fileName = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		openXML();
		#elif UNITY_ANDROID
		var _dir = "/storage/emulated/0/";
		DirectoryInfo currentDirectory = new DirectoryInfo (_dir);
		ProcessDirectory(currentDirectory); 
		#elif UNITY_EDITOR
		fileName = Application.dataPath + "/" + "../../aula_conf.xml";
		openXML();
		#elif UNITY_STANDALONE_OSX
		fileName = Application.dataPath + "/" + "../../aula_conf.xml"; 
		openXML();
		#elif UNITY_STANDALONE_WIN
		fileName = Application.dataPath + "/" + "../../../../aula_conf.xml";
		openXML();
		#endif
		}
}

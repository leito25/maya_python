'''
Created on 14/06/2018

@author: lq-protomind
'''

import sys
from PyQt5.QtWidgets import QApplication, QWidget

if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    
    w = QWidget()
    w.resize(250, 150)
    w.move(300, 300)
    w.setWindowTitle('Demo')
    w.show()
    
    sys.exit(app.exec_())